# Bloggio(Blog platform)
Bloggio is a simple blog platform built on Laravel 5.2.

## Adding correct database information
Edit the `.env` file to put the correct name of the database and correct user information. Bloggio assumes there's no database ready for it so it'll attempt to make a database for itself. If you'd like to change this behaviour, refer to the information below.
It will also assume that it's a development environment so it'll also migrate the tables and seed the database with random rows of data.

## Running in production
If you're using it in production, you might want to tweak the `composer.json` before you run `composer install`.
Specifically, you'd want to refer to [this line](https://bitbucket.org/Ruinisprobablytaken/bloggio/src/a1804fc077324110241d8f924199363fe398f7d8/composer.json?at=master&fileviewer=file-view-default#composer.json-41). These are the commands which will be run after the `composer install` command has finished.