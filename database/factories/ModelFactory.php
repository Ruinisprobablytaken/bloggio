<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Bloggio\Models\User::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->name,
		'email' => $faker->email,
		'password' => bcrypt("secret"),
		'remember_token' => str_random(10),
	];
});

$factory->define(Bloggio\Models\Article::class, function (Faker\Generator $faker) {
	return [
		'header' => $faker->text(60),
		'header_image' => $faker->imageUrl(800, 400),
		'content' => $faker->paragraphs(4, true),
		'user_id' => $faker->numberBetween(1, 40),
	];
});