$.ajaxSetup({
	dataType: "json",
	error: function(e) {
		$("#container").prepend('<p class="text-muted">There is an internal problem. If you keep seeing this message, contact us.</p>');
	}
});