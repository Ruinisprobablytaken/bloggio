;

function render(data) {
	for(var i = 0; i < data.length; i++) {
		$("#listing").append('\
			<li class="item">\
				<h4 class="item_header">' + data[i].header + '</h4>\
				<p class="item_content">' + data[i].content + '</p>\
				<div class="item_footer clearfix">\
					<div class="col-xs-2 col-xs-offset-10">\
						<a class="btn btn-block btn-default" href="' + data[i].id + '">Details</a>\
					</div>\
				</div>\
			</li>\
		');

	}
}

(function($) {
	var loading = false;

	function load(url) {

		loading = true;

		$.ajax({
			type: "get",
			url: url,
			success: function(json) {
				render(json.data);
				next_page_url = json.next_page_url;
			}
		});

		loading = false;
	}

	$(window).scroll(function() {
		if(loading || !next_page_url) {
			return;
		}
		
	   if($(window).scrollTop() + window.innerHeight == $(document).height()) {
			load(next_page_url);
	   }
	});

})(jQuery);