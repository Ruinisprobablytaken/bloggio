<?php

namespace Bloggio\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Bloggio\Models\Article;
use Bloggio\Http\Controllers\Controller;

class ArticleController extends Controller {

	public function getCreate() {
		return view("article.create");
	}

	public function postCreate(Request $request) {
		if($request->wantsJson()) {

		}
	}

	public function getUpdate() {
		return view("article.update");
	}

	public function postUpdate(Request $request) {
		if($request->wantsJson()) {

		}
	}

	public function postDelete(Request $request) {
		if($request->wantsJson()) {

		}
	}

	public function one($id, Request $request) {
		$article = Article::find($id);

		if(!$article) {
			if($request->wantsJson()) {
				return response(trans("errors.404.header"), 404);
			}

			return redirect()->route("errors.404");
		}

		if($request->wantsJson()) {
			return response($article, 200);
		}

		return view("article.details", ["article" => $article]);
	}

	public function all(Request $request) {
		$articles = Article::paginate(15);

		if($request->wantsJson()) {
			return response($articles, 200);
		}
		
		return view("article.list", ["articles" => $articles]);
	}
}
