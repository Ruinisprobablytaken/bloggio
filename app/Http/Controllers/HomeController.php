<?php

namespace Bloggio\Http\Controllers;

use Bloggio\Http\Controllers\Controller;

class HomeController extends Controller {

	public function main() {
		return view("home");
	}
}
