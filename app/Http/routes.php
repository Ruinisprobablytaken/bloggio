<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function() {

	Route::get("/", [
		"uses" => "\Bloggio\Http\Controllers\HomeController@main",
		"as" => "home",
	]);

	Route::get("/register", [
		"uses" => "\Bloggio\Http\Controllers\Auth\AuthController@showRegistrationForm",
		"as" => "register",
		"middleware" => "guest",
	]);

	Route::post("/register", [
		"uses" => "\Bloggio\Http\Controllers\Auth\AuthController@register",
		"middleware" => "guest",
	]);

	Route::get("/login", [
		"uses" => "\Bloggio\Http\Controllers\Auth\AuthController@showLoginForm",
		"as" => "login",
		"middleware" => "guest",
	]);

	Route::post("/login", [
		"uses" => "\Bloggio\Http\Controllers\Auth\AuthController@login",
		"middleware" => "guest",
	]);

	Route::get("/logout", [
		"uses" => "\Bloggio\Http\Controllers\Auth\AuthController@logout",
		"as" => "logout",
		"middleware" => "auth",
	]);

	Route::get("/article/new", [
		"uses" => "\Bloggio\Http\Controllers\ArticleController@getCreate",
		"as" => "article.create",
	]);

	Route::post("/article/new", [
		"uses" => "\Bloggio\Http\Controllers\ArticleController@postCreate",
	]);

	Route::get("/article/update/{id}", [
		"uses" => "\Bloggio\Http\Controllers\ArticleController@getUpdate",
		"as" => "article.update",
	]);

	Route::post("/article/update/{id}", [
		"uses" => "\Bloggio\Http\Controllers\ArticleController@postUpdate",
	]);

	Route::post("/article/delete/{id}", [
		"uses" => "\Bloggio\Http\Controllers\ArticleController@postDelete",
		"as" => "article.delete",
	]);

	Route::get("/article/{id}", [
		"uses" => "\Bloggio\Http\Controllers\ArticleController@one",
		"as" => "article.one",
	]);

	Route::get("/articles", [
		"uses" => "\Bloggio\Http\Controllers\ArticleController@all",
		"as" => "article.all",
	]);

	Route::get("/404", [
		"uses" => function() { return view("errors.404"); },
		"as" => "errors.404",
	]);
});