<?php

namespace Bloggio\Console\Commands;

use PDO;
use PDOException;
use Illuminate\Console\Command;
use Illuminate\Console\AppNamespaceDetectorTrait;

class CreateDB extends Command
{
	use AppNamespaceDetectorTrait;

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'db:create
							{name? : The new database name}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates the database for the app.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$name = strtolower(rtrim($this->getAppNamespace(), "\\"));
		//Console argument > .env DB_DATABASE variable > App namespace + _db
		$schema = $this->argument("name") ?: env("DB_DATABASE", $name . "_db");

		$this->line(sprintf("Making a database under the name %s.", $schema));
		$this->createDatabase($schema);

		if(env("DB_DATABASE") != $schema) {
			$this->updateEnv($schema);
		}
	}

	protected function createDatabase($schema) {
		/**
		 * This might be a really bad decision, but frankly there's no way that I know of to go around Laravel's need to connect to a database instantly other than using PDO directly.
		 */
		try {
			$connection = new PDO(
				sprintf('mysql:host=%s;', config("database.connections.mysql.host")),
				config("database.connections.mysql.username"),
				config("database.connections.mysql.password"));
			$connection->exec(sprintf("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET %s COLLATE %s",
				$schema,
				config("database.connections.mysql.charset"),
				config("database.connections.mysql.collation")
			));
			
			$this->info(sprintf("Database %s created or it already exists.", $schema));
		}
		catch(PDOException $e) {
			$this->error(sprintf("Couldn't create %s database, returned with the error: %s", $schema, $e->getMessage()));
		}
	}

	protected function updateEnv($schema) {
		$path = base_path('.env');

        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                'DB_DATABASE=' . env("DB_DATABASE"), 'DB_DATABASE=' . $schema, file_get_contents($path)
            ));
        }

        $this->info(sprintf(".env updated, database name is now %s.", $schema));
	}
}
