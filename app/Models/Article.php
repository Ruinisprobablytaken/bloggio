<?php

namespace Bloggio\Models;

use Eloquent;

class Article extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header', 'header_image', 'content', 'content_images', 'user_id',
    ];
}
