<?php

return [

    "404" => [
    	"header" => "Not Found",
    	"description" => "We seem to be unable to find the page you're looking for. Make sure you didn't misspell or leave out a part of the URL.",
    	"cta" => [
    		"message" => "If you think this is an internal error, contact our administrator at ",
    	    "email" => env("WEBMASTER_EMAIL", "webmaster@example.com"),
    	    "name" => env("WEBMASTER_NAME", "webmaster"),
    	],
    ],

];
