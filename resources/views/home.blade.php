@extends("templates.default")

@section("extra-assets")
			<script type="text/javascript" src='{{ URL::asset("js/load-more.js") }}'></script>
@stop

@section("page-script")
				$.ajax({
					type: "get",
					url: "{{ route("article.all") }}",
					success: function(json) {
						render(json.data);
						next_page_url = json.next_page_url;
					}
				});
@stop

@section("content")
				<h2 class="page-header">Welcome</h2>
				<ul id="listing"></ul>
				<button id="b">Click me</button>
@stop